#!/usr/bin/perl -w
###
### Author: ron.tapia@ligo.org
### Date: Apr 10, 2020
### Desc: Entrypoint for gstlal container. Adds some environment variables and
###       runs the specified program with arguments.
###
use strict;
my(@args) = @ARGV;
my($prog) = shift(@args);
my($gstlal) = "/gstlal";
my($pkgConfig) = sprintf("%s/lib/pkgconfig:%s/lib64/pkgconfig", $gstlal, $gstlal);
if (defined($ENV{'PKG_CONFIG_PATH'})) {
    $pkgConfig = $pkgConfig . ":" . $ENV{'PKG_CONFIG_PATH'};
}
my($gstreamerPluginPath) = sprintf("%s/lib/gstreamer-1.0", $gstlal);
if (defined($ENV{'GST_PLUGIN_PATH'})) {
    $gstreamerPluginPath = $gstreamerPluginPath . ":" . $ENV{'GST_PLUGIN_PATH'};
}
my($giTypelib) = sprintf("%s/lib/girepository-1.0", $gstlal);
if (defined($ENV{'GI_TYPELIB_PATH'})) {
    $giTypelib = $giTypelib . ":" . $ENV{'GI_TYPELIB_PATH'};
}
$ENV{'TMPDIR'}   = "/tmp";
$ENV{'LAL_PATH'} = $gstlal;
$ENV{'LD_LIBRARY_PATH'} = '/opt/intel/compilers_and_libraries_2020.0.166/linux/mkl/lib/intel64_lin';
$ENV{'PATH'} = sprintf("%s/bin:%s", $gstlal, $ENV{'PATH'});
$ENV{'PYTHONPATH'} = sprintf("%s/lib/python2.7/site-packages:%s/lib64/python2.7/site-packages", $gstlal, $gstlal);
$ENV{'PKG_CONFIG_PATH'} = $pkgConfig;
$ENV{'GST_PLUGIN_PATH'} = $gstreamerPluginPath;
$ENV{'GI_TYPELIB_PATH'} = $giTypelib;
system($prog, @args);
