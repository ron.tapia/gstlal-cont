#!/bin/bash
###
### Author: ron.tapia@ligo.org
### Date: Apr 5, 2020
### Desc: Install LALSuite and GSTLal
### 
### This script installs a specific set of LALSuite and GSTLal source tarballs
### against a specific version of the Intel MKL.
###
### The Intel MKL version must match whaterver is installed.
###
### The LALSuite tarballs were take from:
###
###     https://wiki.ligo.org/Computing/LALSuite
###
### The GSTLal tarballs were taken from:
###
###     https://git.ligo.org/lscsoft/gstlal
###
mkdir /gstlal

XLAL_URL="http://software.ligo.org/lscsoft/source/lalsuite"
XGST_URL="http://software.ligo.org/lscsoft/source"
XTDIR="/gstlal"
export LAL_PATH=${XTDIR}
export PKG_CONFIG_PATH=${LAL_PATH}/lib/pkgconfig:${LAL_PATH}/lib64/pkgconfig:${PKG_CONFIG_PATH}
export GST_PLUGIN_PATH=${LAL_PATH}/lib/gstreamer-1.0:${GST_PLUGIN_PATH}
export PYTHONPATH=${LAL_PATH}/lib/python2.7/site-packages:${LAL_PATH}/lib64/python2.7/site-packages:${PYTHONPATH}
GST_REGISTRY_1_0=${LAL_PATH}/registry.bin
export LDFLAGS="-L/opt/intel/compilers_and_libraries_2020.0.166/linux/mkl/lib/intel64_lin"
export CFLAGS="-I/opt/intel/compilers_and_libraries_2020.0.166/linux/mkl/include"
export LD_LIBRARY_PATH="${LAL_PATH}/lib64:${LAL_PATH}/lib:/opt/intel/compilers_and_libraries_2020.0.166/linux/mkl/lib/intel64_lin":${LD_LIBRARY_PATH}

function LSinstall () {
    echo "###"
    echo "### LSinstall: " $1
    echo "###"
    cd /usr/local/src
    wget ${XLAL_URL}/$1.tar.xz
    tar xvJf $1.tar.xz
    cd $1
    ./configure --prefix=${XTDIR}
    make 
    make install
    cd /usr/local/src
    rm -rf $1.tar.xz $1
}

function GSTinstall () {
    echo "###"
    echo "### GSTinstall: " $1
    echo "###"
    cd /usr/local/src
    wget ${XGST_URL}/$1.tar.gz
    tar xvzf $1.tar.gz
    cd $1
    ./configure --prefix=${XTDIR} $2
    make 
    make install
    cd /usr/local/src
    rm -rf $1.tar.gz $1
}

###
### Install LALSuite
###
XPKGS="lal-6.22.0 lalmetaio-1.6.0 lalframe-1.5.0 lalsimulation-2.0.0 \
       lalburst-1.5.4 lalinspiral-1.10.0 lalpulsar-1.18.2 lalinference-2.0.0 \
       lalapps-6.25.1"

LSinstall lal-6.22.0

#export LAL_LIBS="-L${XTDIR}/lib -llal"
#export LAL_CFLAGS="-I${XTDIR}/include"

for p in ${XPKGS}; do
  LSinstall $p
done

###
### Install GSTLal
###
XPKGS="gstlal-ugly-1.6.6 gstlal-calibration-1.2.11 gstlal-burst-0.2.0"

GSTinstall gstlal-1.5.1

#export GSTLAL_CFLAGS="-I/gstlal/include"
#export GSTLAL_LIBS="-L/gstlal/lib -lgstlal"

for p in ${XPKGS}; do
  GSTinstall $p
done

GSTinstall gstlal-inspiral-1.6.9 --disable-massmodel
