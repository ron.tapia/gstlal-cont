FROM mkl-dev:latest
LABEL maintainer="ron.tapia@ligo.org"
RUN yum -y install gstreamer1-devel.x86_64 gstreamer1-plugins-base-devel.x86_64 python2-pip.noarch \
                   cairo-gobject.x86_64 cairo-gobject-devel.x86_64 object-introspection-devel.x86_64 \
                   pygobject3-devel.x86_64 pycairo.x86_64 pycairo-devel.x86_64 pygobject3-devel.x86_64 \
                   pygobject2-devel.x86_64
WORKDIR /work
ADD scripts/installGstLal.sh /usr/local/src/installGstLal.sh
RUN chmod ugo+rx /usr/local/src/installGstLal.sh && /usr/local/src/installGstLal.sh 2>/var/log/installGstLal.err |  tee /var/log/installGstLal.out 
RUN mkdir -p /usr/local/bin /ligo /cvmfs && chmod ugo+rx /usr/local/bin /ligo /cvmfs
ADD scripts/run.pl /usr/local/bin/run.pl
RUN chmod ugo+rx /usr/local/bin/run.pl
ENTRYPOINT ["/usr/local/bin/run.pl"]
